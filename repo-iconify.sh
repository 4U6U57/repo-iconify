#!/bin/bash

##
# @file repo-iconify.sh
# @brief Generates custom repo icon.png files from user's ~/.face
# @author August Valera
# @version
# @date 2017-11-18
#

# Program variables
Exe=$(basename $0 .sh)
ConfigFile=".$Exe"

# Process command line arguments
for Arg in $@; do
  Tag=$(echo $Arg | cut -d '=' -f 1)
  if grep -q "$Tag" $ConfigFile; then
    echo "Replaced config: $Arg"
    sed -i "s/$Tag=.*/$Arg/g" $ConfigFile
  else
    echo "Added config: $Arg"
    echo $@ >> $ConfigFile
  fi
done

# Error function
Error() {
  echo "$Exe: ERROR: $@" >&2
}

# Check dependences
if ! command -v convert >/dev/null; then
  Error "WARNING: dependency imagemagick not installed"
  sudo apt install imagemagick -y
fi
if ! command -v convert >/dev/null || ! command -v identify >/dev/null; then
  Error "Could not install dependencies"
  exit 1
fi

# Exports variables Width, Height, and MinDim (Minimum Dimension) for use by
# config files and the default settings
GenerateImageData() {
  if [[ -e $InputFile ]]; then
    Width=$(identify -ping -format %w $InputFile)
    Height=$(identify -ping -format %h $InputFile)
    [[ $Width -lt $Height ]] && MinDim=$Width || MinDim=$Height
  fi
}

# Get preferences from configuration files
RecursiveSource() {
  if [[ $1 == "/" ]]; then
    echo "Reached root directory: $1"
  elif [[ -e $1/$ConfigFile ]] && grep -q "\s*Root\s*=\s*true\s*" $1/$ConfigFile; then
    echo "Reached root config: $1/$ConfigFile"
  else
    # Haven't reached a root yet, recurse backward
    RecursiveSource $(dirname $1)
  fi
  # Folding right, sourcing config files from top (root) to bottom (local)
  if [[ -e $1/$ConfigFile ]]; then
    echo "Loaded config: $1/$ConfigFile"
    source $1/$ConfigFile
  fi
}
RecursiveSource $(pwd)

# Default settings, if not set (or unset) by configuration files
# Not included: Text, Prefix, Suffix, Point, Stroke, which are handled below
[[ -z $InputFile ]] && InputFile=~/.face
[[ -z $Color ]] && Color=white
[[ -z $Background ]] && Background=black
[[ -z $Outline ]] && Outline=$Color
[[ -z $Text ]] && Text=""
[[ -z $Direction ]] && Direction=east
[[ -z $Offset ]] && Offset=+0+0
[[ -z $OutputFile ]] && OutputFile=logo.png

# Dynamic default settings
# Some settings are better if they have data on the image
GenerateImageData
[[ -z $Point ]] && Point=$((MinDim * 7 / 10 / 4))
[[ -z $Stroke ]] && Stroke=$((Point / 20))
[[ -z $FullText ]] && FullText="$Prefix$Text$Suffix"

# Sanitize inputs
[[ ! -e $InputFile ]] && Error "File does not exist: $InputFile" && exit
[[ -z $Text ]] && Error "No text specified" && exit

# The actual command
convert $InputFile \
  \( -background $Background -bordercolor $Background -border 15 -fill $Color \
  -stroke $Outline -strokewidth $Stroke -pointsize $Point -gravity south \
  label:"$FullText" \) -gravity $Direction -geometry $Offset -composite $OutputFile

# Success message
if [[ -e $OutputFile ]]; then
  echo "Outputted to $OutputFile"
else
  Error "Error writing to $OutputFile"
  exit 1
fi
